# Getting Started

This project uses the [Poetry](https://poetry.eustace.io/) dependency manager.

First, figure out what command points to your Python 3.6+ installation (if it's not python, replace all occurences of python with that command)  
To install it, if you're on linux, or have exposed linux tools in your Git for Windows install, type this in term/cmd:  
`curl -sSL https://raw.githubusercontent.com/sdispater/poetry/master/get-poetry.py | python`  
If you haven't exposed linux tools on Windows, type this in powershell:
```
wget https://raw.githubusercontent.com/sdispater/poetry/master/get-poetry.py -OutFile get-poetry.py
python get-poetry.py
```

It is also recommended to make sure that poetry is in your path, because you will be using it a lot.

After you have Poetry installed, you can clone the repo and install dependencies with `poetry install`.

# Creating issues
## Bug reports
Bug reports should contain as much information about the bug as possible.  
Version down to the commit hash, operating system, log files, you name it, we probably need it.  
Tell us as much as you can right from the start, because if you don't we may end up asking you anyways.  

## Feature requests
Feature requests should tell us what the feature does, and how it's accessed, maybe,
if you feel adventurous, even implementation proposals.    
If you submit a feature request, be prepared that a long, public, discussion of the request may begin before it's implementation even starts.

## General questions
Whenever you have a question regarding some functionality of the application, and you feel it's not explained in the Wiki or the application itself,
feel free to ask us in the issues. That way, we also know what needs to be added to the application/wiki so further asking of the same question is unnecessary.

# Updating code

Before you even start, make sure that all dependencies install flawlessly, and running pytests passes without error.  
If it doesn't, and you know how to fix that, immediatelly open an issue, and get to fixing it. We'll gladly accept a fix that doesn't break existing codebase.

Once you're all set up, what you should do, is find an issue that you'd like to solve - 
make sure it's within your field of expretise, and maybe discuss the issue with the author first.  
Because no one likes waiting a month for a fix, that doesn't work.

If you're adding a feature, make sure to write tests for it. Maybe even write tests before you begin writing the feature.

Once you're done fixing the issue, make sure that all tests and lints pass on your machine before creating a merge request back to this repo.

# Getting your code merged

We don't have any special requirements for what a merge request should look like but,
you should be prepared for your changes to be challenged by developers and maintainers.

